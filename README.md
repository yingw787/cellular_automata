# `cellular_automata`: Python implementations of various cellular automata.

## Quick Start

1. Create a `conda` environment from `environment.yml`.
2. Activate `conda` environment using `conda activate cell`.
3. Execute cellular automata simulation using `python $SIMULATION.py`.
