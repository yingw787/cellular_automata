"""A simple Python implementation of Brian's Brain, visualized with
`matplotlib`.

Description of Brian's Brain: https://en.wikipedia.org/wiki/Brian%27s_Brain

Author: Ying Wang
Date: October 4th, 2018
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


N = 100

ON = 255
DYING = 127
OFF = 0
states = [
    ON,
    DYING,
    OFF
]

# Populate grid.
grid = np.random.choice(
    states,
    N * N,
    p=[0.2, 0, 0.8]
).reshape(N, N)

def update(data):
    """Updates the grid.

    Args:
        data (float)
    """
    global grid
    # Copy to ensure clean state update.
    new_grid = grid.copy()

    for x_idx in range(N):
        for y_idx in range(N):
            on_neighbors = 0

            if grid[x_idx, (y_idx - 1) % N] == ON:
                on_neighbors += 1
            if grid[x_idx, (y_idx + 1) % N] == ON:
                on_neighbors += 1
            if grid[(x_idx - 1) % N, y_idx] == ON:
                on_neighbors += 1
            if grid[(x_idx + 1) % N, y_idx] == ON:
                on_neighbors += 1
            if grid[(x_idx - 1) % N, (y_idx - 1) % N] == ON:
                on_neighbors += 1
            if grid[(x_idx - 1) % N, (y_idx + 1) % N] == ON:
                on_neighbors += 1
            if grid[(x_idx + 1) % N, (y_idx - 1) % N] == ON:
                on_neighbors += 1
            if grid[(x_idx + 1) % N, (y_idx + 1) % N] == ON:
                on_neighbors += 1

            if grid[x_idx, y_idx] == ON:
                new_grid[x_idx, y_idx] = DYING
            elif grid[x_idx, y_idx] == DYING:
                new_grid[x_idx, y_idx] = OFF
            elif grid[x_idx, y_idx] == OFF and on_neighbors == 2:
                new_grid[x_idx, y_idx] = ON

    matrix.set_data(new_grid)
    grid = new_grid
    return [matrix]

# Set up animation.
figure, axes = plt.subplots()
matrix = axes.matshow(grid)
ani = animation.FuncAnimation(figure, update, interval=250, save_count=50)

plt.show()
